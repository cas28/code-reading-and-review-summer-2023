<html>
<head>
<style>
  body { max-width: 8in; margin: 1in; }
  table { border: 1px solid black; border-collapse: collapse; }
  th, td { border: 1px solid black; padding: 0.3em; }
  th { text-align: left; }
  td { text-align: center; }
</style>
</head>
<body>

<h1>CS 410P/510 Code Reading &amp; Review, Summer 2023</h1>
<h2>Syllabus</h2>

<h3>Academic misconduct</h3>
  <p>All assignments are <b>individual work</b> unless the assignment explicitly says that group work is allowed.</p>

  <p>When you submit individual work, you are implicitly claiming that <b>you alone</b> are the author of the content that you are submitting, except any content that you have explicitly cited a source for. If this claim is not true, your submission is plagiarism.</p>

  <p>If you <b>implicitly or explicitly</b> claim authorship of anything that you did not personally write, you will get a grade of zero on your submission. If you do it more than once, you will immediately fail the course and I will write you up for academic misconduct.</p>

  <p>Review PSU's rules at <a href="https://www.pdx.edu/dos/academic-misconduct">https://www.pdx.edu/dos/academic-misconduct</a>.</p>

<h3>Repository</h3>
  <div>The syllabus and schedule for this course will be tracked in a GitLab repository at <a href="https://gitlab.cecs.pdx.edu/cas28/code-reading-and-review-summer-2023">https://gitlab.cecs.pdx.edu/cas28/code-reading-and-review-summer-2023</a>. This will let you see the history of any changes that I may make to these documents throughout the quarter.</div>

<h3>Course staff</h3>
  <p>Please <a href="mailto:cas28@pdx.edu,kphilip@pdx.edu">email <b>both of us</b></a> if you have a question!</p>
  <table>
    <tr><td>
      <div>Katie Casamento</div>
      <div>Email: cas28@pdx.edu</div>
      <div>Office hours: Wednesday 3-4pm, in FAB 115D or at <a href="https://pdx.zoom.us/j/89740476106">https://pdx.zoom.us/j/89740476106</a></div>

      <p>FAB 115D is in the CS offices behind the fishbowl, near the bottom-right of page 9 in the <a href="https://www.pdx.edu/buildings/sites/g/files/znldhr2301/files/2021-10/Fourth%20Avenue%20Building%20Floorplans.pdf">building plan</a> (where L120-00 is the fishbowl).</p>
    </td></tr>
    <tr><td>
      <div>Katherine Philip</div>
      <div>Email: kphilip@pdx.edu</div>
      <div>Office hours: TBA</div>
    </td></tr>
  </table>


<h3>Lecture</h3>
  <p>Lecture is Monday and Wednesday 6:00-8:20PM, at <a href="https://pdx.zoom.us/j/83374267353">https://pdx.zoom.us/j/83374267353</a>.</p>
  <p>Lecture recordings will be available on Canvas within 48 hours of each live lecture. <b>In-class discussion and Zoom chat will be recorded!</b> The recordings will only be available to students enrolled in this course for this quarter.</p>
  <p>Please keep your microphone muted when you're not talking so it doesn't pick up noise in the environment.</p>

<h3>Reading</h3>
  <p>There will be required readings posted to Canvas throughout the quarter.</p>

<h3>Course discussion forum</h3>
  <p>We'll be using the <a href="https://fishbowl.zulip.cs.pdx.edu/">Zulip server hosted by the CAT at PSU</a> for course discussion. You should have already received an invite to the <a href="https://fishbowl.zulip.cs.pdx.edu/#narrow/stream/215-crr-summer-2023">course discussion stream</a>. If you haven't received an invite, please email me!

<h3>Assignments</h3>
  <p>There will be four major assignments, which will attempt to simulate parts of a (somewhat) realistic collaborative software development process, working in a (somewhat) realistic development environment. The assignment details will be posted to the Canvas and discussed in lecture.</p>
  <p>The assignments will primarily use the <a href="https://www.typescriptlang.org/">TypeScript</a> language. In the first two weeks of lecture, we'll spend some time introducing TypeScript and getting everyone set up with a development environment. I recommend using an IDE for these assignments, especially if you've never worked with an IDE before; I'll walk through setup instructions for Visual Studio Code in lecture, but if you already have a favorite IDE, you can probably easily install a TypeScript plugin for it.</p>
  <p>I strongly recommend <strong>not</strong> doing all of your work remotely on the CS department Linux servers for this course. Instead, try to follow along in the first couple weeks of lecture when we talk about setting up a development environment, and set up your environment directly on your computer. If you don't have a suitable computer to work on, you may be able to <a href="https://library.pdx.edu/study-spaces-computers/equipment/">borrow a laptop from the PSU library</a>.</p>

<h3>Quizzes</h3>
  <p>Each week, there will be a "code review quiz" on Canvas designed to give you practice in the topics that we've discussed in lecture. These will usually involve both writing in English and editing some provided code in TypeScript.</p>
  <p>Quiz responses will usually be graded on how well you justify your answers, not on whether you get the "right answers". There will often be certain answers that I expect, but it's fine if your answer is different than the one I expect as long as you can justify it.</p>

<h3>Discussion prompts</h3>
  <p>There will be weekly discussion prompts on Zulip to encourage discussion about the weekly reading. This discussion is not graded, but it is will hopefully be valuable in broadening your understanding of the topics we cover!</p>

<h3>Course project</h3>
  <p>Graduate students are required to complete a course project. You may work alone or in groups of two or three, but no more than three.</p>
  <p>For the course project, you will select a <b>large codebase that at least one member of your group has previously worked on</b>. Your goal is to <b>significantly improve the quality and maintainability of the codebase, according to the principles that we cover in class</b>.</p>
  <p>At the end of the course, each graduate student will submit two project deliverables:
    <ul>
      <li>A link to a Git repository with your group's work, hosted on a site like GitLab or GitHub.</li>
      <li>A writeup explaining your own individual contributions to the project.</li>
    </ul>
  </p>

<h3>Deadlines</h3>
  <p>See the course schedule document on Canvas for the release dates and due dates of the assigned coursework.</p>
  <p>The deadlines for the quizzes and course project are each "hard due dates": you must submit by these dates in order to get credit for your work.</p>
  <p>The assignment deadlines are "soft due dates". If you submit an assignment or quiz by its soft due date, you will get a grade and feedback on your submission within one week of the soft due date.</p>
  <p>Assignment 4 is an exception: there won't be time for resubmissions since it's due at the end of the quarter.</p>
  <p>If you submit an assignment after the soft due date, I guarantee that your submission will be graded for full credit by the end of the quarter, but I'll grade it whenever it's convenient for me. That's the deal!</p>

<h3>Resubmissions</h3>
  <p>As an incentive to submit assignments "on time", you have a limited opportunity to resubmit for an updated grade after your submission has been graded. <b>If you submit an assignment by its soft due date</b>, then after you get back your grade, you may resubmit once for an updated grade.</p>
  <p>A resubmission has a hard due date of two weeks after the original soft due date. This leaves you with at least one week to rework your submission after receiving a grade and feedback on it.</p>
  <p>Resubmissions after this hard due date will not be graded, and if you don't submit an assignment by its soft due date, then resubmissions will not be graded at all for that assignment.</p>

<h3>Grading</h3>
  <p>Grading will be very subjective for this course, since most of the material is subjective. For each piece of work you submit, you will receive feedback and a letter grade from A to D. Grades will be recorded on the Canvas page for this course. Your final grade will be based on an the letter grades you've received, curved up at my discretion.</p>
  <table>
    <tr><th></th><th scope="col">Undergrad</th><th scope="col">Grad</th></tr>
    <tr><th scope="row">Assignments</th><td>70%</td><td>50%</td></tr>
    <tr><th scope="row">Quizzes</th><td>30%</td><td>20%</td></tr>
    <tr><th scope="row">Project</th><td>N/A</td><td>30%</td></tr>
  </table>

</body>
</html>
