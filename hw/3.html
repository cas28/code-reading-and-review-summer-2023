<h2>Project goals</h2>
<p>As we've discussed, most real-world software work is maintenance work: most developers spend most of their careers maintaining codebases that already exist, not creating new codebases from the ground up.</p>
<p>Maintenance work broadly involves fixing bugs, adding features, and organizing the code so that it will be easier to fix bugs and add features in the future. That third activity is often called <i>refactoring</i>. When we refactor a codebase, we try to improve its structure <b>without</b> changing its behavior from the user's perspective.</p>
<p>Careless refactoring can be counter-productive! Developers get used to the structure of the code that they work on, and every time it changes they have to spend time re-learning it. The term "churn" is often used to describe an unproductive work process where code is frequently changing but not meaningfully improving.</p>
<p>Codebases usually <b>need</b> some amount of refactoring over time in order to stay maintainable, but we should always plan and execute our refactoring efforts carefully in order to minimize the need for more refactoring in the near future. A refactoring effort should "pay for itself" in the form of increased future productivity.</p>
<p>In this project, you will begin <b>planning</b> a refactoring effort to <b>improve the extensibility</b> of an existing codebase.</p>

<h2>Setup</h2>
<p>Download the code from <a href="https://gitlab.cecs.pdx.edu/cas28/robosnake">https://gitlab.cecs.pdx.edu/cas28/robosnake</a>, either from the download button or by cloning with Git.</p>
<p>Read the README page at the GitLab link and run the game in your browser a couple times. This is the codebase that you will be planning to refactor, so you will need to understand what it does!</p>

<h2>Analyzing the code</h2>
<p>There are <b>three</b> related concepts in the AI code for this project: <i>snakes</i>, <i>agents</i>, and <i>players</i>.</p>
<ul>
  <li>A <i>snake</i> is the data representing the position and score of a snake in the game. Snakes are represented by the <code>SnakeState</code> type.</li>
  <li>An <i>agent</i> is some code that controls how a snake moves. There is currently no single type that represents an agent, but the agent code is found in <code>Agent.ts</code>.</li>
  <li>A <i>player</i> is a human being who is competing in the game by providing agent code to control a snake. Players are represented by the <code>Player</code> type, like how players are represented with the symbols X and O in tic-tac-toe. Each <code>Player</code> value corresponds to a specific color, like how in checkers there is a red side and a black side.</li>
</ul>
<p>Focus on how these concepts interact in the <code>Agent.ts</code> code. Notice that there is currently no explicit separation between agents and players in the code: conceptually we would say "each player <b>has an</b> agent in the game", but this is not modeled as a "has-a" relationship in the code.</p>
<p>Imagine the scenario of users playing this game. Each player will probably work on their agent code individually, and then four players will bring their four agents together into a single program to play a game against each other.</p>
<p>With the current structure of the codebase, agent code is not very "portable" in this way. The players might have to do significant work in order to produce a version of the game that incorporates the changes all four of them have made to the codebase while writing their agent code. This is not ideal!</p>

<h2>Planning the refactoring</h2>
<p>Ideally, there should be an explicit <code>Agent</code> type, and each player should be able to bring a single piece of <b>encapsulated</b> code that defines a value of the <code>Agent</code> type. This will be the fundamental goal of your refactoring. To demonstrate the extensibility of your design, you will produce some example code illustrating how it supports some non-trivial agents.</p>

<p>Submit your work to this Canvas page <b>with the specified file names</b>. For this assignment, you do not need to modify the existing code in the codebase at all.</p>
<ol>
  <li>In a file named <code>AgentType.ts</code>, write a TypeScript type definition for an <code>Agent</code> type that will allow agent code to be more portable as described above. You should probably use an <code>interface</code> type, unless you have some experience in functional programming and you want to try a functional solution.</li>
  <li>In a file named <code>Plan.txt</code>, <b>briefly describe</b> how the code in <code>Agent.ts</code> and <code>GameRunner.ts</code> will have to change in order to work with your new <code>Agent</code> type. Use short code samples to illustrate your points, but don't write out all of the code changes explicitly yet.</li>
  <li>In a file named <code>RightAgent.ts</code>, define a value of your <code>Agent</code> type that always moves right, like the hard-coded behavior of player A in the original code.</li>
  <li>In a file named <code>TuesdayAgent.ts</code>, define a value of your <code>Agent</code> type that always moves up on Tuesdays and always moves down on other days of the week, using the <code>js-joda</code> library from assignment 2. (You can install <code>js-joda</code> in the <code>robosnake</code> project by running <code>npm i --save-dev @js-joda/core</code> in the project directory.)</li>
  <li>In a file named <code>CycleAgent.ts</code>, define a value of your <code>Agent</code> type that cycles through the moves up, up, right, down, right, like the hard-coded behavior of player C in the original code.<code>CycleAgent.ts</code>. <b>Do not use any global variables to store the cycle or index.</b></li>
  <li>In a file named <code>StairAgent.ts</code>, define a value of your <code>Agent</code> type that moves left one time, up one time, left two times, up two times, left three times, up three times, etc. The number of moves in each direction should <b>keep increasing</b> until the player has lost. Your definition should work correctly for <b>any size game screen</b>, not just the default 50x50 game screen. <b>Do not use any global variables to store numbers or directions.</b></li>
</ol>
